package agenda.model.repository.classes;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Contact;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RepositoryContactFileTest {

    RepositoryContactFile repositoryContactFile;

    @Before
    public void setUp() throws Exception {
        repositoryContactFile = new RepositoryContactFile();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = NullPointerException.class)
    public void addContactTest1() throws Exception{
        Contact contact = new Contact(null, null, null);
        repositoryContactFile.addContact(contact);
    }

    @Test(expected = InvalidFormatException.class)
    public void addContactTest2() throws Exception{
        Contact contact = new Contact("name1","adress","");
        repositoryContactFile.addContact(contact);
    }

    @Test(expected = InvalidFormatException.class)
    public void addContactTest3() throws Exception{
        Contact contact = new Contact("name1","adress","+");
        repositoryContactFile.addContact(contact);
    }

    @Test(expected = InvalidFormatException.class)
    public void addContactTest4() throws Exception{
        Contact contact = new Contact("name1","adress","123");
        repositoryContactFile.addContact(contact);
    }

    //invalid name
    @Test(expected = InvalidFormatException.class)
    public void addContactTest5() throws Exception{
        Contact contact = new Contact("n","adress","+0123");
        repositoryContactFile.addContact(contact);
    }

    @Test(expected = InvalidFormatException.class)
    public void addContactTest6() throws Exception{
        Contact contact = new Contact("","adress","+0123");
        repositoryContactFile.addContact(contact);
    }

    //invalid name
    @Test(expected = InvalidFormatException.class)
    public void addContactTest7() throws Exception{
        String string ="";
        for(int i=0;string.length()<=256;i++)
            string += 'c';

        Contact contact = new Contact(string,"adress","+0123");
        repositoryContactFile.addContact(contact);
    }

    @Test(expected = InvalidFormatException.class)
    public void addContactTest8() throws Exception{
        Contact contact = new Contact("name1","adress","+012345678912341233413123123123");
        repositoryContactFile.addContact(contact);
    }

    @Test
    public void addContactTest9() throws Exception{
        int count = repositoryContactFile.getContacts().size();
        Contact contact = new Contact("name1","adress","+012345678");
        repositoryContactFile.addContact(contact);
        if (repositoryContactFile.count()-1 != count) {
            assert (false);
        }
    }


}
