package agenda.model.repository.classes;

import agenda.model.base.Activity;
import agenda.model.base.Contact;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class RepositoryActivityFileTest {

    RepositoryActivityFile repositoryActivityFile;
    RepositoryContactFile repositoryContactFile;

    @Before
    public void setUp() throws Exception {
        repositoryContactFile = new RepositoryContactFile();
        repositoryActivityFile = new RepositoryActivityFile(repositoryContactFile);
    }

    @Test
    public void addNullActivity() {
        assertFalse( repositoryActivityFile.addActivity(null));
    }

    @Test
    public void addActivityWithConflict() {
        Contact contact = repositoryContactFile.getContacts().get(0);
        List<Contact> contacts = new ArrayList<>();
        contacts.add(contact);

        Date startDate = new Date(Long.parseLong("1363775400000"));
        Date endDate = new Date(Long.parseLong("1363777200000"));

        Activity activity = new Activity("user1",startDate,endDate,contacts,"description");
//        System.out.println(activity.getStart() + " " +activity.getDuration());
        assertFalse(repositoryActivityFile.addActivity(activity));
    }

    @Test
    public void addActivity() {
        Contact contact = repositoryContactFile.getContacts().get(0);
        List<Contact> contacts = new ArrayList<>();
        contacts.add(contact);

        Date startDate = new Date();
        Date endDate = new Date(startDate.getTime() + 1000*60*60);

        Activity activity = new Activity("user1",startDate,endDate,contacts,"description");
//        System.out.println(activity.getStart() + " " +activity.getDuration());
        assertTrue(repositoryActivityFile.addActivity(activity));
    }

    @Test
    public void activitiesOnDate1() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2013,3-1,20);
        Date date = calendar.getTime();

        List<Activity> activities = repositoryActivityFile.activitiesOnDate("name1",date);

        assertNotEquals(activities.size(),0);

        activities.forEach(x->{
            Calendar calendarX = Calendar.getInstance();
            calendarX.setTime(x.getStart());
            assertEquals(calendarX.get(Calendar.YEAR),calendar.get(Calendar.YEAR));
            assertEquals(calendarX.get(Calendar.MONTH),calendar.get(Calendar.MONTH));
            assertEquals(calendarX.get(Calendar.DAY_OF_MONTH),calendar.get(Calendar.DAY_OF_MONTH));
        });
    }

    @Test
    public void activitiesOnDate2() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2022,3-1,20);
        Date date = calendar.getTime();
        List<Activity> activities = repositoryActivityFile.activitiesOnDate("name1",date);
        assertEquals(activities.size(),0);
    }
}